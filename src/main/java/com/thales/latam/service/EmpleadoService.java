package com.thales.latam.service;

import java.util.List;

import com.thales.latam.dto.EmpleadoDTO;

public interface EmpleadoService {
	
	public List<EmpleadoDTO> consultaEmpleado(String idEmpleado);
	public List<EmpleadoDTO> consultaEmpleados();

}
