package com.thales.latam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.thales.latam.dao.EmpleadoDao;
import com.thales.latam.dto.EmpleadoDTO;
import com.thales.latam.service.EmpleadoService;

public class EmpleadoServiceImpl implements EmpleadoService{
	
	@Autowired
	EmpleadoDao empleadoDao;
	
	  public List<EmpleadoDTO> consultaEmpleados (){
		  return empleadoDao.consultaEmpleados();
	   }
	   
	   public List<EmpleadoDTO> consultaEmpleado (String idEmpleado){
		  return empleadoDao.consultaEmpleado(idEmpleado);
	   }

}