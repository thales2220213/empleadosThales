package com.thales.latam.entity;

import java.io.*;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Empleado implements Serializable{

   private static final long serialVersionUID = -3522620467169972705L;
   
   private String id;
   private String nombre;
   private String apellido;
   private String direccion;
   private String cargo;
   private Date fechaIngreso;
	
   public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCargo() {
		return cargo;
	}
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	public Date getFechaIngreso() {
		return fechaIngreso;
	}
	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
   
  
   
}