package com.thales.latam.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thales.latam.dto.EmpleadoDTO;
import com.thales.latam.service.EmpleadoService;

@RequestMapping(value = {"v1"})
public class EmpleadoController{
	
	 @Autowired
	 EmpleadoService empleadoService;

	 @GetMapping(value = {"/employees"})
	 public List<EmpleadoDTO> consultaEmpleados(){
		 return empleadoService.consultaEmpleados();
     }
	 
	 @GetMapping(value = {"/employee/"})
	 public List<EmpleadoDTO> consultaEmpleados(String idEmpleado){
		 return empleadoService.consultaEmpleado(idEmpleado);
     }
}