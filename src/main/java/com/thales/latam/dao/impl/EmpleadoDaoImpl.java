package com.thales.latam.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import com.thales.latam.dao.EmpleadoDao;
import com.thales.latam.dto.EmpleadoDTO;
import com.thales.latam.entity.Empleado;



public class EmpleadoDaoImpl implements EmpleadoDao{
	
	public List<EmpleadoDTO> consultaEmpleados(){
		EntityManagerFactory emf =
				Persistence.createEntityManagerFactory("PERSON");
		EntityManager em = emf.createEntityManager();
	    List<EmpleadoDTO> listEmployees = new ArrayList<EmpleadoDTO>();
		
	    try {
			TypedQuery<EmpleadoDTO> query =
			em.createQuery("SELECT * FROM EMPLOYEES EMP", EmpleadoDTO.class);
		    listEmployees= query.getResultList();
		} catch (Exception e) {
		    e.printStackTrace();
		} finally {
			em.close();
			 
		}  
	    return listEmployees;
	}
	   
	public List<EmpleadoDTO> consultaEmpleado (String idEmpleado){
		  EntityManagerFactory emf =
					Persistence.createEntityManagerFactory("PERSON");
			EntityManager em = emf.createEntityManager();
		    List<EmpleadoDTO> listEmployeesDto = new ArrayList<EmpleadoDTO>();
		    List<Empleado> listEmployees = new ArrayList<Empleado>();
			
		    try {
				TypedQuery<Empleado> query =
				em.createQuery("SELECT * FROM EMPLOYEES EMP WHERE EMP.ID = ?", Empleado.class);
				query.setParameter(1, idEmpleado);
			    listEmployees=  query.getResultList();
			    
				for (Empleado empleado: listEmployees) {
					EmpleadoDTO empleadoDto = new EmpleadoDTO();
					empleadoDto.setApellido(empleado.getApellido());
					empleadoDto.setCargo(empleado.getCargo());
					empleadoDto.setDireccion(empleado.getDireccion());
					empleadoDto.setFechaIngreso(empleado.getFechaIngreso());
					empleadoDto.setId(empleado.getId());
					empleadoDto.setNombre(empleado.getNombre());
					listEmployeesDto.add(empleadoDto);
				}
			} catch (Exception e) {
			    e.printStackTrace();
			} finally {
				em.close();
				 
			}	 
		   return listEmployeesDto;
	}

}