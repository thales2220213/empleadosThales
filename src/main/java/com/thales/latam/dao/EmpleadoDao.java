package com.thales.latam.dao;

import java.util.List;

import com.thales.latam.dto.EmpleadoDTO;

public interface EmpleadoDao {

	public List<EmpleadoDTO> consultaEmpleado(String idEmpleado);
	public List<EmpleadoDTO> consultaEmpleados();
}
